input=$1
for f in $(jq .response | jq -r 'keys[]') ; do
    re='^[0-9]{6}$'
    if [[ $f =~ $re ]] ; then
        country=$(jq -r ".response.\"$f\".flagUrl" $input | rev | cut -d'/' -f1 | rev | cut -d'-' -f1)
        echo "$country (id=$f) successfully processed."
        jq ".response.\"$f\"" $input | jq ".country = \"$country\"" | jq ".id = \"$f\"" > "countries/$f-$country.json"
        jq -r ".response.\"$f\".content" $input | tidy -q -i -m -w 160 -ashtml -utf8 > "countries/$f-$country.html"
    fi
done < $input
